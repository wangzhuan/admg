package model

type AdUnit struct {
	AdUnitId      int64  `json:"ad_unit_id"`
	AdUnitName    string `json:"ad_unit_name"`
	AdUnitType    int    `json:"ad_unit_type"`
	AppId         int64  `json:"app_id"`
	AppName       string `json:"app_name"`
	HasExperiment int    `json:"has_experiment"`
	OsType        string `json:"os_type"`
	SegmentItems  []struct {
		ExperimentGroup int64  `json:"experiment_group"`
		HasExperiment   bool   `json:"has_experiment"`
		IsDefault       bool   `json:"is_default"`
		SegmentId       int64  `json:"segment_id"`
		SegmentName     string `json:"segment_name"`
		SegmentStatus   int    `json:"segment_status"`
	} `json:"segment_items"`
	Status int `json:"status"`
}

type BaseResp[T any] struct {
	Code    string `json:"code"`
	Message string `json:"message"`
	Data    T      `json:"data"`
}

type App struct {
	AppId   int64  `json:"app_id"`
	AppName string `json:"app_name"`
	OsType  string `json:"os_type"`
	Status  int    `json:"status"`
}

type Segment struct {
	AdUnitId                    int64          `json:"ad_unit_id"`
	CreateTime                  int64          `json:"create_time"`
	Data                        string         `json:"data"`
	DataObj                     *SegmentFilter `json:"data_obj"`
	ExperimentDetailId          int64          `json:"experiment_detail_id"`
	ExperimentId                int64          `json:"experiment_id"`
	ExperimentType              int            `json:"experiment_type"`
	HasExperiment               int            `json:"has_experiment"`
	HasReadyExperiment          bool           `json:"has_ready_experiment"`
	HasReadyOrRunningExperiment bool           `json:"has_ready_or_running_experiment"`
	HasRunningExperiment        bool           `json:"has_running_experiment"`
	HasValidCode                bool           `json:"has_valid_code"`
	HasWaterfallExperiment      int            `json:"has_waterfall_experiment"`
	IsDefault                   bool           `json:"is_default"`
	Name                        string         `json:"name"`
	NeedRecommendedSettings     bool           `json:"need_recommended_settings"`
	Priority                    int64          `json:"priority"`
	SegmentId                   int64          `json:"segment_id"`
	Status                      int            `json:"status"`
	WaterfallId                 []int64        `json:"waterfall_id"`
}

type SegmentFilter struct {
	Custom []*Custom `json:"custom"`
}
type Custom struct {
	Filter int64 `json:"filter"`
	Value  []struct {
		Filter int64  `json:"filter"`
		Name   string `json:"name"`
		Value  string `json:"value"`
	} `json:"value"`
}
type Waterfall struct {
	AdSlotList    []*AdSlot `json:"ad_slot_list"`
	AdUnitId      int64     `json:"ad_unit_id"`
	AdUnitName    string    `json:"ad_unit_name"`
	LayerTimeout  int64     `json:"layer_timeout"`
	ParallelScale int       `json:"parallel_scale"`
	ParallelType  int       `json:"parallel_type"`
	TotalTimeout  int64     `json:"total_timeout"`
}
type AdSlot struct {
	AdSlotId                   string  `json:"ad_slot_id"`
	AdSlotName                 string  `json:"ad_slot_name"`
	BiddingType                int     `json:"bidding_type"`
	Currency                   string  `json:"currency"`
	Id                         int64   `json:"id"`
	Network                    int     `json:"network"`
	Price                      float64 `json:"price,omitempty"`
	ShowSort                   int     `json:"show_sort"`
	SortType                   int     `json:"sort_type"`
	Status                     int     `json:"status"`
	OriginType                 int     `json:"origin_type"`
	SmartSortSwitch            int     `json:"smart_sort_switch"`
	SmartSortFetchDays         int     `json:"smart_sort_fetch_days"`
	SmartSortEffectiveStatus   int     `json:"smart_sort_effective_status"`
	SmartSortIneffectiveReason string  `json:"smart_sort_ineffective_reason"`
}
