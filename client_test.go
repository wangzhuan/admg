package admg

import (
	"encoding/json"
	"fmt"
	"testing"
)

func TestClient_QueryMediationApp(t *testing.T) {
	client := NewClient(105057, 105057, "4a751fc3a00e030fb61bb5c17c73c0946934eb9a7d894df5ee0e81a83f0147e4")
	fmt.Println(json.Marshal(client.QueryMediationApp()))
}

func TestClient_QueryMediationAdUnits(t *testing.T) {
	client := NewClient(105057, 105057, "4a751fc3a00e030fb61bb5c17c73c0946934eb9a7d894df5ee0e81a83f0147e4")
	marshal, e := json.Marshal(client.QueryMediationAdUnits(5311516))
	fmt.Printf("%v,%v", string(marshal), e)
}

func TestClient_QueryMediationOperateSegment(t *testing.T) {
	client := NewClient(105057, 105057, "4a751fc3a00e030fb61bb5c17c73c0946934eb9a7d894df5ee0e81a83f0147e4")
	marshal, e := json.Marshal(client.QueryMediationOperateSegment(102108341))
	fmt.Printf("%v,%v", string(marshal), e)
}

func TestClient_QueryMediationWaterfallDetail(t *testing.T) {
	client := NewClient(105057, 105057, "4a751fc3a00e030fb61bb5c17c73c0946934eb9a7d894df5ee0e81a83f0147e4")
	marshal, e := json.Marshal(client.QueryMediationWaterfallDetail(102108341, 56978))
	fmt.Printf("%v,%v", string(marshal), e)
}
