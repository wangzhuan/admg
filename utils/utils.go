package utils

import (
	"bytes"
	"crypto/md5"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"sort"
)

func SortAndJoint(params map[string]string) string {
	//对key进行排序
	var sortedKeys []string
	for k, _ := range params {
		sortedKeys = append(sortedKeys, k)
	}
	sort.Strings(sortedKeys)

	//拼接待签名字符串
	bufWaitSign := bytes.NewBufferString("")
	for _, key := range sortedKeys {
		bufWaitSign.WriteString(fmt.Sprintf("%s=%s&", key, params[key]))
	}
	bufWaitSign.Truncate(bufWaitSign.Len() - 1)

	return bufWaitSign.String()
}

func Sign(str string) string {
	sum := md5.Sum([]byte(str))
	return fmt.Sprintf("%x", sum)
}

func HttpGet[T any](host, pathStr string, params map[string]string, resp T) error {
	urll, err := url.Parse(host)
	if err != nil {
		return err
	}

	values := make(url.Values)
	for k, v := range params {
		values.Add(k, v)
	}
	urll.RawQuery = values.Encode()
	urll.Path = pathStr

	urlStr := urll.String()

	httResp, err := http.Get(urlStr)
	if err != nil {
		return err
	}

	respByte, err := io.ReadAll(httResp.Body)
	if err != nil {
		return err
	}
	err = json.Unmarshal(respByte, resp)
	if err != nil {
		return err
	}

	return nil
}
