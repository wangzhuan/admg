package consts

const HOST = "https://www.csjplatform.com"

const (
	QUERY_MEDIATION_APP              = "union_media/open/api/mediation/app"
	QUERY_MEDIATION_ADUNITS          = "union_media/open/api/mediation/adunits"
	QUERY_MEDIATION_OPERATE_SEGMENT  = "union_pangle/open/api/mediation/operate_segment"
	QUERY_MEDIATION_WATERFALL_DETAIL = "/union_media/open/api/mediation/waterfall_detail"
)

const (
	SignTypeMd5 = "MD5"
)

type SignType string
