package admg

import (
	"encoding/json"
	"fmt"
	"gitlab.com/wangzhuan/admg/consts"
	"gitlab.com/wangzhuan/admg/model"
	"gitlab.com/wangzhuan/admg/utils"
	"time"
)

type Client struct {
	userId      int64
	roleId      int64
	securityKey string
	signType    consts.SignType
	version     string
}

func NewClient(userId int64, roleId int64, securityKey string) *Client {
	return &Client{userId: userId, roleId: roleId, securityKey: securityKey, signType: consts.SignTypeMd5, version: "2.0"}
}

func (c *Client) QueryMediationApp() []*model.App {

	params, err := c.getRequest(map[string]any{})
	if err != nil {
		return nil
	}

	type AppList struct {
		AppList []*model.App `json:"app_list"`
	}
	resp := &model.BaseResp[AppList]{}

	err = utils.HttpGet[*model.BaseResp[AppList]](consts.HOST, consts.QUERY_MEDIATION_APP, params, resp)
	if err != nil {
		return nil
	}
	return resp.Data.AppList
}

func (c *Client) QueryMediationAdUnits(appId int64) []*model.AdUnit {

	params, err := c.getRequest(map[string]any{"app_id": appId})
	if err != nil {
		return nil
	}

	type AdUnitList struct {
		AdUnitList []*model.AdUnit `json:"ad_unit_list"`
	}
	resp := &model.BaseResp[AdUnitList]{}

	err = utils.HttpGet[*model.BaseResp[AdUnitList]](consts.HOST, consts.QUERY_MEDIATION_ADUNITS, params, resp)
	if err != nil {
		return nil
	}
	return resp.Data.AdUnitList
}

func (c *Client) QueryMediationOperateSegment(adUnitId int64) []*model.Segment {

	params, err := c.getRequest(map[string]any{
		"mode":       2,
		"sort_type":  2,
		"ad_unit_id": adUnitId,
	})
	if err != nil {
		return nil
	}

	resp := model.BaseResp[[]*model.Segment]{}

	err = utils.HttpGet[*model.BaseResp[[]*model.Segment]](consts.HOST, consts.QUERY_MEDIATION_OPERATE_SEGMENT, params, &resp)
	if err != nil {
		return nil
	}

	for _, d := range resp.Data {
		sf := &model.SegmentFilter{}
		json.Unmarshal([]byte(d.Data), sf)
		d.DataObj = sf
	}
	return resp.Data
}

func (c *Client) QueryMediationWaterfallDetail(adUnitId int64, segmentId int64) *model.Waterfall {

	params, err := c.getRequest(map[string]any{
		"ad_unit_id": adUnitId,
		"segment_id": segmentId,
	})
	if err != nil {
		return nil
	}

	resp := model.BaseResp[*model.Waterfall]{}

	err = utils.HttpGet[*model.BaseResp[*model.Waterfall]](consts.HOST, consts.QUERY_MEDIATION_WATERFALL_DETAIL, params, &resp)
	if err != nil {
		return nil
	}

	return resp.Data
}

// getRequest 获取请求对象
func (c *Client) getRequest(params any) (map[string]string, error) {
	paramsWaitSign := map[string]string{}

	if tmp, ok := params.(map[string]string); ok {
		paramsWaitSign = tmp
	}

	if tmp, ok := params.(map[string]any); ok {
		for k, v := range tmp {
			paramsWaitSign[k] = fmt.Sprintf("%v", v)
		}
	} else {
		marshal, err := json.Marshal(params)
		if err != nil {
			return nil, err
		}
		tmpParams := make(map[string]any)
		err = json.Unmarshal(marshal, &tmpParams)
		if err != nil {
			return nil, err
		}
		for k, v := range tmpParams {
			paramsWaitSign[k] = fmt.Sprintf("%v", v)
		}
	}

	paramsWaitSign["user_id"] = fmt.Sprintf("%d", c.userId)
	paramsWaitSign["role_id"] = fmt.Sprintf("%d", c.roleId)
	paramsWaitSign["timestamp"] = fmt.Sprintf("%d", time.Now().UnixMilli())
	paramsWaitSign["sign_type"] = string(c.signType)
	paramsWaitSign["version"] = c.version

	waitSignString := utils.SortAndJoint(paramsWaitSign)
	signString := utils.Sign(waitSignString + c.securityKey)

	paramsWaitSign["sign"] = signString

	return paramsWaitSign, nil
}
